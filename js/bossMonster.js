class BossMonster{
    isDead
    health;
    level;
    attackDamage;
    moveSpeed;
    sx;
    sy;
    sw
    sh;
    dx;
    dy;
    dw;
    dh;
    img;
    character;
    score;
    attacks;
    der;
    isAttack;
    stopMove;
    imgUpdateSize;


    constructor(level, character){
        this.name = "boss";
        this.isDead = false;
        this.level = level;
        this.attacks = [];
        this.isAttack = false;
        this.stopMove = false;
        this.img = IMG_bossMonster_spritesheet;
        this.statSetting();
        this.character = character; //to check current character's position
    }

    statSetting(){
        if(this.level === 1){ 
            this.score = 1;
            this.health = 200000;
            this.attackDamage = 50;
            this.moveSpeed = 1;
            this.imgUpdateSize = 960;
        }
        else if(this.level === 2){ 
            this.score = 1;
            this.health = 400000;
            this.attackDamage = 50;
            this.moveSpeed = 0.5;
            this.imgUpdateSize = 960;
        }
        else if(this.level === 3){ 
            this.score = 5;
            this.health = 8000;
            this.attackDamage = 10;
            this.moveSpeed = 1.5;
            this.imgUpdateSize = 960;
        }
        else if(this.level === 4){ 
            this.score = 1;
            this.health = 8000;
            this.attackDamage = 5;
            this.moveSpeed = 8.5;
            this.imgUpdateSize = 960;
        }

        else{
            
        }
        this.positionSetting();
    }

    positionSetting(){
        let rPos = this.randomPosition();
        this.sw = 240;
        this.sh = 240;
        this.sx = 0;
        this.sy = this.level * 240 - 240;
        this.dw = 240;
        this.dh = 240;
        this.dx = rPos[0];
        this.dy = rPos[1];
    }

    randomPosition(){
        let rx = Math.floor(Math.random() * global_canvasWidth);
        let ry = Math.floor(Math.random() * global_canvasHeight);
        let rr = Math.random();
        if(rr < 0.25){
            rx = 0;
        }
        else if(rr < 0.5){
            rx = global_canvasWidth;
        }
        else if(rr < 0.75){
            ry = 0;
        }
        else{
            ry = global_canvasHeight;
        }
        return [rx, ry];
    }

    damaged(AD){
        if(this.level === 5 && this.hide === true){
            return;
        }
        this.health -= AD;
        this.imageUpdateHit();
        this.checkHealth();
        if(this.isDead){ 
            return -1;
        }
        else{
            return 1;
        }

    }

    checkHealth(){
        if(this.health <= 0){
            this.isDead = true;
        }
    }

    move(){   
        this.setDerivativePosition();
        if(!this.stopMove){
            this.dx = this.dx + this.der[0];
            this.dy = this.dy + this.der[1];
        }
    }

    setDerDirection(){
        if(this.dx - this.character.dx >= 0){
            this.der[0] = this.der[0] * -1;
        }

        if(this.dy - this.character.dy >= 0){
           this.der[1] = this.der[1] * -1;
        }
    }

    setDerivativePosition(){
        let diffX = this.character.dx - this.dx;
        let diffY = this.character.dy - this.dy;
        let digonalDistance = Math.sqrt(diffX**2 + diffY**2);
        if(digonalDistance === 0){
            return [0, 0];
        }
        let derX = Math.sqrt(((this.moveSpeed * diffX) / digonalDistance)**2);
        let derY = Math.sqrt(((this.moveSpeed * diffY) / digonalDistance)**2);

        this.der = [derX, derY];
        this.setDerDirection();

    }

    attack(){
        if(this.isAttackValid()){
            if(this.level === 1){
                this.miniMonster = 5;
            }
        }
    }

    isAttackValid(){
        if(this.level === 1){
            if(this.sx === 960 && !this.isAttack){
                this.isAttack = true;
                this.stopMove = true;
                return true;
            }
        }
        return false;
    }

    imageUpdate(){
        if(this.level === 1){
            if(this.sx >= this.imgUpdateSize){
                this.sx = 0;
                this.stopMove = false;
            }
            else{
                this.sx += 240;
            }
        }

        else if(this.level === 2){
            if(this.sx >= this.imgUpdateSize){
                this.sx = 0;
                this.stopMove = false;
            }
            else{
                this.sx += 240;
            }
        }

        else if(this.level === 3){
            if(this.sx >= this.imgUpdateSize){
                this.isAttack = false;
                this.stopMove = false;
                this.sx = 0;
            }
            else{
                this.sx += 240;
            }  
        }
     
        else if(this.level === 4){
            if(this.sx >= this.imgUpdateSize){
                this.sx = 0;
                this.stopMove = false;
            }
            else{
                this.sx += 240;
            }
        }
       
        else if(this.level === 5){
            if(this.sx >= this.imgUpdateSize){
                this.isAttack = false;
                this.stopMove = false;
                this.sx = 0;
            }
            else{
                if(this.sx >= 160){
                    this.stopMove = true;
                }
                if(this.sx === 240 && this.holdPoint < 10){;
                    this.holdPoint++;
                    this.hide = true;
                }
                else{
                    this.holdPoint = 0;
                    this.hide = false;
                    this.sx += 80;
                }
            }
        }

        else if(this.level === 6){
            
        }

        else if(this.level === 7){
            
        }
    }

    imageUpdateHit(){
        this.sx = 1200;
        this.stopMove = true;
    }

    events(){   
        this.move();
        this.attack();
        this.removeOutOfRangeAttacks();
        this.attacksMove();
        this.drawAttacks();
    }

    removeOutOfRangeAttacks(){
        this.attacks = this.attacks.filter(attack => (!attack.isOutOfRange && !attack.hit));
    }

    drawMonster(){
        global_canvas.context.drawImage(this.img, this.sx, this.sy, this.sw, this.sh, this.dx, this.dy, this.dw, this.dh);

    }
    drawAttacks(){
        this.attacks.forEach(attack => attack.drawAttack());
    }

    attacksMove(){
        this.attacks.forEach(attack => attack.move());
    }
}