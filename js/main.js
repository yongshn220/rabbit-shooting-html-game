var requestId;
var player1;

let stage;
let storage;
let gameOver = false;

$(document).ready(function(){

    player1 = new Player('A');
    storage = new Storage();
    animate();
    addEventListener(); 
    console.log(player1.stage.monsterRespondTime);
    
})

function newItem(){
    if(player1.stage.curStage % 3 === 0){
        player1.character.stats.skill.addSkillPoint();
        player1.character.stats.gainHealthPoint(100);
    }
    //player1.newItem("health");
    //player1.newItem("health");
}

timePlayerOne = {
    start : 0,
    elapsed : 0,
    step : 200,
}

monsterTime = {
    start : 0,
    elapsed : 0,
    respond : 0,
    gapTime : 5000,
    curGapTime : 0
}


function animate(now = 0){
    timePlayerOne.elapsed = now - timePlayerOne.start;
    monsterTime.elapsed = now - monsterTime.start;
    player1.stage.showInfo();
    //walking motion event
    if(timePlayerOne.elapsed > timePlayerOne.step){
        timePlayerOne.start = now;
        playersImageUpdate();
    }

    if(monsterTime.elapsed >= monsterTime.respond){
        monsterTime.start = now;
        monsterTime.respond = player1.stage.monsterRespondTime * 1;
        if(player1.newMonster()){
           //nothing 
        }
        else{
            if(player1.isAllCleared()){
                console.log("cleared");
                monsterTime.curGapTime += 1000;
            }
            console.log(monsterTime.curGapTime)
            if(monsterTime.curGapTime >= monsterTime.gapTime){
                monsterTime.curGapTime = 0;
                player1.stage.gapTime = false;
                player1.stage.nextStage();
                newItem();
            }
        }
    }

    //new life event
    playersClearCanvas();
    playersAnimation();
    if(!gameOver){
        requestId = requestAnimationFrame(animate);
    }
}


function playersGameStateCheck(){
    if(!player1.gameState){
        cancelAnimationFrame(requestId);
    }
}

function playersImageUpdate(){
    player1.character.imageUpdate();
    player1.monstersImageUpdate();
    player1.character.isDamaged = false;
}

function playersAnimation(){
    playersHealthPointCheck();
    playersDrawFirstBackground();

    player1.showAttacks();
    player1.showItems(); 
    player1.showTexts();
    player1.stage.showStage();
    player1.showMonsters();
    player1.showCharacter();
    monsterEvent();
    player1.characterAndMonsterNotCollied();
    player1.isMonsterAttackHitCharacter();
    

}

function monsterEvent(){
    player1.monstersEvents();
}

function playersClearCanvas(){
    global_canvas.clearCanvas();
}

function playersDrawFirstBackground(){
    player1.showBaseBackground();
    //player1.showMiddleBackground();
}

function playersDrawLastBackground(){
   // player1.showTopBackground();
}

function playersHealthPointCheck(){
    if(!player1.checkState()){
        gameOver = true;
        let name = prompt("Enter your name.");
        let stage = this.player1.stage.curStage;
        while(name === "" || name === null || name.length > 10){
            if(name === null){
                return;
            }
            name = prompt("Too long or too short. Please enter your name again.");
        }
        storage.addRank(name, stage);
        storage.showRanks();
        return;
    }
}

function resetRank(){
    var pw = prompt("Password", "");
    if(pw === "yongjung"){
        storage.resetAll();
        alert("Successfully reset.")
    }
    else if(pw === "remove pos"){
        var rn = prompt("Which position do you want to remove?", "");
        storage.removeNum(rn)
    }
    else{
        alert("Wrong password!");
    }
}
