class Attack{
    sx;              // attack fired position. to check range;
    sy;              // attack fired position
    x;               //x position in canvas
    y;
    aw;              // attack width size
    ah;              // attack height size
    speed;
    acc;             // acceleration
    direction;
    shotDirection;
    range;
    attackDamage;
    img;
    hitImg;
    skill;

    constructor(x, y, direction, skill, attackLevel){
        
        this.x = x;
        this.y = y;
        this.speed = 10;
        this.acc = 1;
        this.direction = direction;
        this.skill = skill;
        this.img = new Image();
        this.hitImg = new Image();
        this.attackPositionAdjust();
        this.setAttackInfo(attackLevel);

    }

    attackPositionAdjust(){
        if(this.direction === "left"){
            this.x = this.x - 0;
            this.y = this.y + 45;
        }
        else if(this.direction === "up"){
            this.x = this.x + 48;
        }
        else if(this.direction === "right"){
            this.x = this.x + 60;
            this.y = this.y + 45;
        }
        else if(this.direction === "down"){
            this.x = this.x + 23;
            this.y = this.y + 70;
        }
        else if(this.direction === "up-left"){
            this.x = this.x + 15;
            this.y = this.y + 25;
        }
        else if(this.direction === "up-right"){
            this.x = this.x + 54;
            this.y = this.y + 40;
        }
        else if(this.direction === "down-left"){
            this.x = this.x + 12;
            this.y = this.y + 50;
        }
        else if(this.direction === "down-right"){
            this.x = this.x + 30;
            this.y = this.y + 58;
        }
        this.sx = this.x; 
        this.sy = this.y;
    }

    move(){
        this.speed = this.speed * this.acc;
        let der = Math.sqrt(this.speed**2 / 2);
        if(this.direction === "left"){
            this.x = this.x - this.speed
        }
        else if(this.direction === "up"){
            this.y = this.y - this.speed;
        }
        else if(this.direction === "right"){
            this.x = this.x + this.speed;
        }
        else if(this.direction === "down"){
            this.y = this.y + this.speed;
        }
        else if(this.direction === "down-left"){
            this.x = this.x - der;
            this.y = this.y + der;
        }
        else if(this.direction === "down-right"){
            this.x = this.x + der;
            this.y = this.y + der;
        }
        else if(this.direction === "up-left"){
            this.x = this.x - der;
            this.y = this.y - der;
        }
        else if(this.direction === "up-right"){
            this.x = this.x + der;
            this.y = this.y - der;
        }
    }

    moveNum(x, y){
        this.x = this.x + x;
        this.y = this.y + y;
    }

    setAttackInfo(level){
        this.speed = 10;
        this.acc = 1.025;
        
        this.img.src = `css/images/attacks/attack-lev-${level}-${this.skill.attackDamageLevel}.png`;
        if(level === 1){
            this.aw = 10;
            this.ah = 10;
            //this.hitImg.src = ;
        }
        else if(level === 2){
            this.aw = 15;
            this.ah = 15;
        }
        else if(level === 3){
            this.aw = 20;
            this.ah = 20;
        }
        else if(level === 4){
            this.aw = 10;
            this.ah = 10;
        }

        this.setAttackDamage();
        this.setAttackDistance();
    }

    setAttackDamage(){
        this.attackDamage = 200 * (1.7**this.skill.attackDamageLevel) ;
    }
    
    setAttackDamageLevelEvent(){
        this.img.src = 'css/images/attacks/attack-lev-1.png';
    }

    setAttackDistance(){
        this.range = 2000;
    }

    drawAttack(){
        global_canvas.context.drawImage(this.img, this.x, this.y, this.aw, this.ah); 
    }
}