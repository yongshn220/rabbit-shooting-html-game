class MonsterAttack{
    level; 
    sx;
    sy;
    sw
    sh;
    dx;
    dy;
    dw;
    dh;
    speed;
    changeX;
    changeY;
    img;
    isOutOfRange;
    damage;
    hit;
    updateValid;

    constructor(level, dx, dy, der){
        this.changeX = 0;
        this.changeY = 0;
        this.updateValid = false;
        this.isOutOfRange = false;
        this.hit = false;
        this.level = level;
        this.dx = dx;
        this.dy = dy;
        this.setting(der);
    }

    setting(der){
        this.sx = 0;
        this.sy = 0;

        if(this.level === 1){
            this.sw = 10;
            this.sh = 10;
            this.dw = 10;
            this.dh = 10;
            this.speed = 1;
            this.damage = 1;
        }
        else if(this.level === 2){
            this.sw = 10;
            this.sh = 10;
            this.dw = 10;
            this.dh = 10;
            this.speed = 1;
            this.damage = 1;
        }
        else if(this.level === 3){ // 달팽이
            this.img = IMG_monsterAttack3;
            this.sw = 10;
            this.sh = 10;
            this.dw = 10;
            this.dh = 10;
            this.speed = 15;
            this.damage = 10;
            this.changeX = der[0] * -1;
            this.changeY = der[1] * -1;
        }
        else if(this.level === 4){
            this.sw = 10;
            this.sh = 10;
            this.dw = 10;
            this.dh = 10;
            this.speed = 10;
            this.damage = 10;
        }
        else if(this.level === 5){
            this.sw = 10;
            this.sh = 10;
            this.dw = 10;
            this.dh = 10;
            this.speed = 10;
            this.damage = 10;
        }
        else if(this.level === 6){  // 서제민 몬스터
            this.img = IMG_monsterAttack6;
            this.dx = this.dx + Math.random() * 80 - 40;
            this.dy = this.dy + Math.random() * 80 - 40;
            this.sw = 20;
            this.sh = 20;
            this.dw = 20;
            this.dh = 20;
            this.speed = 0;
            this.damage = 10;
            this.presTime = 0;
            this.updateValid = true;
        }
        else{
            
        }
    }

    checkAttackRange(){
        if(this.dx < 0 || this.dy < 0 || this.dx > global_canvasWidth || this.dy > global_canvasHeight){
            this.isOutOfRange = true;
        }
        if(this.level === 6){
            if(this.presTime > 20){
                this.isOutOfRange = true;
            }
        }
    }

    move(){
        if(this.level === 3){
            this.dx = this.dx - (this.changeX * this.speed);
            this.dy = this.dy - (this.changeY * this.speed);
        }
        else if(this.level === 6){
            if(this.presTime > 15 && this.updateValid){
                this.updateImage();
            }
            this.presTime++;
        }
        this.checkAttackRange();
    }

    updateImage(){
        if(this.level === 6){
            this.sx = this.sx + this.sw;
            this.updateValid = false;
        }
    }

    drawAttack(){
        global_canvas.context.drawImage(this.img, this.sx, this.sy, this.sw, this.sh, this.dx, this.dy, this.dw, this.dh);
    }
}