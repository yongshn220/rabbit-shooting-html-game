
class Monster{
    isDead
    health;
    level;
    attackDamage;
    moveSpeed;
    sx;
    sy;
    sw
    sh;
    dx;
    dy;
    dw;
    dh;
    img;
    character;
    score;
    attacks;
    der;
    isAttack;
    stopMove;
    imgUpdateSize;
    isMini;

    constructor(level, character, isMini){
        this.isDead = false;
        this.level = level;
        this.moveSpeed = 1;
        this.attacks = [];
        this.isAttack = false;
        this.stopMove = false;
        this.isMini = isMini;
        this.img = IMG_monster_spritesheet;  //spritesheet image of monsters
        this.statSetting();
        this.character = character; //to check current character's position
    }

    statSetting(){
        if(this.level === 1){  //초록 애벌래 
            this.score = 50;
            this.health = 2000;
            this.attackDamage = 10;
            this.moveSpeed = 2;
            this.imgUpdateSize = 80;

        }
        else if(this.level === 2){ // 둥근 애벌래
            this.score = 100;
            this.health = 5000;
            this.attackDamage = 10;
            this.moveSpeed = 1.5;
            this.imgUpdateSize = 80;
        }
        else if(this.level === 3){ // 달팽이
            this.score = 500;
            this.health = 8000;
            this.attackDamage = 10;
            this.moveSpeed = 0.5;
            this.imgUpdateSize = 320;
            this.skillStep = 0;
        }
        else if(this.level === 4){ // 막대기
            this.score = 700;
            this.health = 5000;
            this.attackDamage = 5;
            this.moveSpeed = 7.5;
            this.imgUpdateSize = 80;
        }
        else if(this.level === 5){ // 파랑 애벌래
            this.score = 1000;
            this.health = 15000;
            this.attackDamage = 20;
            this.moveSpeed = 5.5;
            this.imgUpdateSize = 320;
            this.holdPoint = 0;
            this.hide = false;
        }
        else if(this.level === 6){ //버프 몹
            this.score = 1500;
            this.health = 15000;
            this.attackDamage = 10;
            this.moveSpeed = 0.5;
            this.imgUpdateSize = 320;
            this.holdPoint = 0;
            this.skillStep = 0;
        }
        else if(this.level === 7){
            this.score = 6000000;
            this.health = 300000;
            this.attackDamage = 10;
            this.moveSpeed = 0.5;
            this.imgUpdateSize = 80;
        }
        
        else{
            
        }

        if(false){
            this.score = 0;
            this.health = this.health;
            this.moveSpeed = 5.0;
            this.attackDamage = this.attackDamage / 4;
        }
        this.positionSetting();
    }

    positionSetting(){
        let rPos = this.randomPosition();
        this.sw = 80;
        this.sh = 80;
        this.sx = 0;
        this.sy = this.level * 80 - 80;
        this.dw = 80;
        this.dh = 80;
        this.dx = rPos[0];
        this.dy = rPos[1];
    }

    randomPosition(){
        let rx = Math.floor(Math.random() * global_canvasWidth);
        let ry = Math.floor(Math.random() * global_canvasHeight);
        let rr = Math.random();
        if(rr < 0.25){
            rx = 0;
        }
        else if(rr < 0.5){
            rx = global_canvasWidth;
        }
        else if(rr < 0.75){
            ry = 0;
        }
        else{
            ry = global_canvasHeight;
        }
        return [rx, ry];
    }

    damaged(AD){
        if(this.level === 5 && this.hide === true){
            return;
        }
        this.health -= AD;
        this.imageUpdateHit();
        this.checkHealth();
        if(this.isDead){
            return this.score;
        }
        else{
            return Math.floor(this.score / 100);
        }
    }

    checkHealth(){
        if(this.health <= 0){
            this.isDead = true;
        }
    }

    move(){   
        this.setDerivativePosition();
        if(!this.stopMove){
            this.dx = this.dx + this.der[0];
            this.dy = this.dy + this.der[1];
        }
    }

    setDerDirection(){
        if(this.dx - this.character.dx >= 0){
            this.der[0] = this.der[0] * -1;
        }

        if(this.dy - this.character.dy >= 0){
           this.der[1] = this.der[1] * -1;
        }
    }

    setDerivativePosition(){
        let diffX = this.character.dx - this.dx;
        let diffY = this.character.dy - this.dy;
        let digonalDistance = Math.sqrt(diffX**2 + diffY**2);
        if(digonalDistance === 0){
            return [0, 0];
        }
        let derX = Math.sqrt(((this.moveSpeed * diffX) / digonalDistance)**2);
        let derY = Math.sqrt(((this.moveSpeed * diffY) / digonalDistance)**2);

        this.der = [derX, derY];
        this.setDerDirection();

    }

    attack(){
        let attackNumber = this.isAttackValid()
        if(attackNumber){
            if(attackNumber === 3){
                let newDer = [this.der[0] / this.moveSpeed, this.der[1] / this.moveSpeed]
                this.attacks.push(new MonsterAttack(this.level, this.dx, this.dy, newDer));
            }
            else if(attackNumber === 6){
                this.attacks.push(new MonsterAttack(this.level, this.charPrevX + 30, this.charPrevY + 60, []))
            }
        }
    }
    
    isAttackValid(){
        if(this.level === 3){
            if(this.sx === 320 && !this.isAttack){
                this.isAttack = true;
                this.stopMove = true;
            }
            else{
                return false;
            }
        }

        else if(this.level === 6){
            if(this.sx === 320 && !this.isAttack){
                this.isAttack = true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        return this.level;
    }

    imageUpdate(){
        //초록 애벌래
        if(this.level === 1){
            if(this.sx >= this.imgUpdateSize){
                this.sx = 0;
                this.stopMove = false;
            }
            else{
                this.sx += 80;
            }
        }
        //둥근 애벌래
        else if(this.level === 2){
            if(this.sx >= this.imgUpdateSize){
                this.sx = 0;
                this.stopMove = false;
            }
            else{
                this.sx += 80;
            }
        }
        //달팽이
        else if(this.level === 3){
            if(this.skillStep < 5){
                if(this.sx >= 80){
                    this.sx = 0;
                    this.skillStep++;    
                }
                else{
                    this.sx += 80;
                }
            }
            else{
                if(this.sx >= this.imgUpdateSize){
                    this.isAttack = false;
                    this.stopMove = false;
                    this.sx = 0;
                    this.skillStep = 0;
                }
                else{
                    this.sx += 80;
                }
            }  
        }
        //막대기
        else if(this.level === 4){
            if(this.sx >= this.imgUpdateSize){
                this.sx = 0;
                this.stopMove = false;
            }
            else{
                this.sx += 80;
            }
        }
        //파랑 애벌래
        else if(this.level === 5){
            if(this.sx >= this.imgUpdateSize){
                this.isAttack = false;
                this.stopMove = false;
                this.sx = 0;
            }
            else{
                if(this.sx >= 160){
                    this.stopMove = true;
                }
                if(this.sx === 240 && this.holdPoint < 10){;
                    this.holdPoint++;
                    this.hide = true;
                }
                else{
                    this.holdPoint = 0;
                    this.hide = false;
                    this.sx += 80;
                }
            }
        }

        else if(this.level === 6){
            if(this.skillStep < 2){
                if(this.sx >= 80){
                    this.sx = 0;
                    this.skillStep++;    
                }
                else{
                    this.sx += 80;
                }
            }
            else{
                if(this.sx >= this.imgUpdateSize){
                    this.isAttack = false;
                    this.stopMove = false;
                    this.sx = 0;
                    this.skillStep = 0;
                }
                else{
                    if(this.sx === 80){
                        this.charPrevX = this.character.dx;
                        this.charPrevY = this.character.dy;
                    }
                    if(this.sx >= 160){
                        this.stopMove = true;
                        if(this.sx === 160){
                            
                        }
                    }
                    this.sx += 80;
                }
            }
        }

        else if(this.level === 7){
            
        }
    }

    imageUpdateHit(){
        this.sx = 400;
        this.stopMove = true;
    }

    events(){   
        this.move();
        this.attack();
        this.removeOutOfRangeAttacks();
        this.attacksMove();
        this.drawAttacks();
    }

    removeOutOfRangeAttacks(){
        this.attacks = this.attacks.filter(attack => (!attack.isOutOfRange && !attack.hit));
    }

    drawMonster(){
        global_canvas.context.drawImage(this.img, this.sx, this.sy, this.sw, this.sh, this.dx, this.dy, this.dw, this.dh);

    }
    drawAttacks(){
        this.attacks.forEach(attack => attack.drawAttack());
    }

    attacksMove(){
        this.attacks.forEach(attack => attack.move());
    }
}