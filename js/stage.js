class Stage{
    gapTime;
    curStage;
    curStageTotalScore;
    curStageCurScore;
    isBossStage;
    startRange;
    endRange;
    monsterList;
    character;
    monsterRespondTime;
    maxRange;
    constructor(character){
        this.gapTime = false;
        this.curStage = 0;
        this.startRange = 1;
        this.endRange = 2;
        this.curStageTotalScore = 1000;
        this.curStageCurScore = 0;
        this.isBossStage = false;
        this.monsterList = [];
        this.character = character;   // to check character's current position. 
        this.monsterRespondTime = 1000;
        this.maxRange = 7;
        this.nextStage();
        this.setDebugScore(this.curStage);
    }

    setDebugScore(num){
        for(let i = 0; i < this.curStage; i++){
            this.curStageTotalScore = Math.floor(this.curStageTotalScore * 1.2);
        }
    }
    
    setStage(){
        if(this.curStage >= 0 && this.curStage < 3){
            this.startRange = 1;
            this.endRange = 1;
        }
        else if(this.curStage >= 3 && this.curStage < 6){
            this.startRange = 1;
            this.endRange = 2;
        }
        else if(this.curStage >= 6 && this.curStage < 10){
            this.startRange = 1;
            this.endRange = 3;
        }
        else if(this.curStage === 10){
            this.startRange = 1;
            this.endRange = 3;
        }
        else if(this.curStage >= 11 && this.curStage < 14){
            this.startRange = 2;
            this.endRange = 4;
        }
        else if(this.curStage >= 13 && this.curStage < 18){
            this.startRange = 2;
            this.endRange = 5;
        }
        else if(this.curStage >= 18 && this.curStage < 20){
            this.startRange = 3;
            this.endRange = 6;
        }
        else if(this.curStage === 20){
            
        }

        //this.startRange = 5;
        //this.endRange = 1;
    }

    setScore(){
        this.curStageTotalScore = Math.floor(this.curStageTotalScore * 1.2);
    }

    addScore(score){
        this.curStageCurScore += score;
    }

    getMonsterLevel(){
        //return 3;
        return Math.floor(Math.random() * (this.endRange - this.startRange + 1) + this.startRange) ;

    }

    nextStage(){
        this.curStageCurScore = 0;
        this.curStage++;
        this.setStage();
        this.setScore();
        this.setMonsterRespondTime();
        this.setMonsterList();
    }
    checkScore(){
        return this.curStageCurScore >= this.curStageTotalScore;
    }
    
    setMonsterList(){
        // calling boss monster.
        if(this.curStage === 10){
            let newBossMonster = new BossMonster(1, this.character);
            this.monsterList.push(newBossMonster);
            this.isBossStage = true;
            this.monsterRespondTime = 700;
        }

        else{
            for(let i = 0; i < this.curStageTotalScore;){
                let newMonster = new Monster(this.getMonsterLevel(), this.character, false);
                i += newMonster.score;
                this.monsterList.push(newMonster);
            }
        }
    }

    getNextMonsterFromList(){
        if(this.monsterList[0] != undefined){
            let nM = this.monsterList[0];
            this.monsterList.splice(0, 1);
            return nM;
        }
        else{
            if(!this.isBossStage){
                this.gapTime = true;
            }
            return false;
        }
    }
    getMiniMonster(){
        return new Monster(this.getMonsterLevel(), this.character, true);
    }

    setMonsterRespondTime(){
        this.monsterRespondTime = 1000;
    }

    getInformation(){
        console.log(`stage : ${this.curStage}`);
        console.log(`totalScore : ${this.curStageTotalScore}`);
        console.log(`CurScore : ${this.curStageCurScore}`);
        console.log(`gapTime : ${this.gapTime}`);
        console.log(`startRange : ${this.startRange}`)
        console.log(`respond : ${this.monsterRespondTime}`)
        console.log("-----------------------------------")
    }

    showStage(){
        let txt = `stage : ${this.curStage}`;
        global_canvas.context.font = "60px Arial";
        global_canvas.context.fillStyle = "#00000029";
        global_canvas.context.textAlign = "center";
        global_canvas.context.fillText(txt, 800, 400);
    }

    showInfo(){
        let target = (document).querySelector("#target");
        let current = (document).querySelector("#current");

        target.innerHTML = `tar : ${this.curStageTotalScore}`;
        current.innerHTML = `no : ${this.curStageCurScore}`;
    }

}