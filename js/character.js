class Character{
    isDamaged;
    name;
    type;
    sx;
    sy;
    sw;
    sh;
    dx;
    dy;
    dw;
    dh;
    image;
    view;
    isMoving;
    isAttacking;
    stats;
    background;

    constructor(name, background){
        this.isDamaged = false;
        this.name = name;
        this.isMoving = false;
        this.isAttacking = false;
        this.image = IMG_character_spritesheet;
        this.view = "down";
        this.stats = new Stats(name, 200);
        this.background = background;
        this.setPosition();
    }

    damaged(AD){
        this.stats.damaged(AD);
    }

    getOppositeView(){
        if(this.view === "left"){
            return "right";
        }
        else if(this.view === "right"){
            return "left";
        }
        else if(this.view === "up"){
            return "down";
        }
        else if(this.view === "down"){
            return "up";
        }
        else{
            
        }
    }

    setPosition(){
        this.dx = 760;
        this.dy = 360;
        this.sx = 0;
        this.sy = 0;
        this.sw = 80;
        this.sh = 80;
        this.dw = 80;
        this.dh = 80;
    }

    imageUpdate(){
        if(this.isMoving){
            if(this.sx >= 240){
                this.sx = 0;
            }
            else{
                this.sx += 80;
            }
        }
        else{
            this.sx = 0;
        }
    }

    viewUpdate(view){
        this.view = view;
            if(view === "down"){
                this.sy = 0;
            }
            else if(view === "up"){
                this.sy = 80;
            }
            else if(view === "left"){
                this.sy = 160;
            }
            else if(view === "right"){
                this.sy = 240;
            }
            else if(view === "down-right"){
                this.sy = 320;
            }
            else if(view === "down-left"){
                this.sy = 400;
            }
            else if(view === "up-left"){
                this.sy = 480
            }
            else if(view === "up-right"){
                this.sy = 560
            }
        
    }

    dead(){
        this.sx = this.sy;
        this.sy = 0;
    }

    newLife(curLife){
        this.stats.state = true;
        if(this.type === "player"){
            this.newPosition();
        }
        this.stats = new Stats(this.name, this.type, curLife);
        var curWepaon = this.stats.inventory.curWeapon.name;
    }

    newPosition(){
        var x;
        var y;
        var notCollied = false;

        while(!notCollied){
            x = Math.floor(Math.random() * 1900);
            y = Math.floor(Math.random() * 1900);

            var tempXL = Math.floor(x / 40);
            var tempXR = Math.floor((x + this.dw) / 40);
            var tempYU = Math.floor(y / 40);
            var tempYD = Math.floor((y + this.dh) / 40);
            if(this.background.wallMap[tempYU][tempXL] ||
            this.background.wallMap[tempYU][tempXR] || 
            this.background.wallMap[tempYD][tempXL] || 
            this.background.wallMap[tempYD][tempXR]){
                notCollied = false;
            }
            else{
                notCollied = true;
                this.gx = x;
                this.gy = y;
                this.dPositionAdjust(this.gx, this.gy);
                this.background.x = this.gx - this.dx;
                this.background.y = this.gy - this.dy;
            }
        }
    }

    dPositionAdjust(gx, gy){
        var dx;
        var dy;

        if(gx < 400){
            dx = gx
        }
        else if(gx > 1600){
            dx = gx - 1199;
        }
        else{
            dx = 400;
        }
        if(gy < 400){
            dy = gy;
        }
        else if(gy > 1600){
            dy = gy - 1399;
        }
        else{
            dy = 400;
        }

        this.dx = dx;
        this.dy = dy;
    }

    drawCharacter(){
        global_canvas.context.drawImage
        (this.image,
         this.sx, 
         this.sy, 
         this.sw, 
         this.sh, 
         this.dx, 
         this.dy, 
         this.dw, 
         this.dh);
    }
}