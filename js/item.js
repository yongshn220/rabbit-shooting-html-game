class Item{
    name;
    x;
    y;
    width;
    height;
    image;

    constructor(name){
        this.x = Math.floor(Math.random() * (global_canvasWidth - 200) + 100);
        this.y = Math.floor(Math.random() * (global_canvasHeight - 200) + 100);
        this.width = 40;
        this.height = 40;
        this.image = new Image();
        this.name = name
        this.setItem();
        
    }

    setItem(){
        if(this.name === "health"){
            this.image.src = 'css/images/item-weapon.png';
        }
        else{
            this.image.src = 'css/images/item-armor.png';
        }
    }

    drawItem(){
        global_canvas.context.drawImage(this.image, 0, 0, this.width, this.height, this.x, this.y, this.width, this.height);
    }
}