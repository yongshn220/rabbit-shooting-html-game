class Skill{
    attackDamageLevel;
    healthPointIncreaseLevel;
    attackLevel;
    attackDamageMaxLevel;
    healthPointIncreaseMaxLevel;
    attackMaxLevel;
    skillPoint;
    stat;

    constructor(name, stat){
        this.name = name;
        this.stat = stat;
        this.attackDamageLevel = 1;
        this.healthPointIncreaseLevel = 1;
        this.attackLevel = 1;
        this.attackDamageMaxLevel = 4;
        this.healthPointIncreaseMaxLevel = 4;
        this.attackMaxLevel = 8;
        this.skillPoint = 0;
        this.showInfo();

    }
    

    addSkillPoint(){
        this.skillPoint++;
        this.showInfo();
    }

    removeSkillPoint(){
        if(this.skillPoint > 0){
            this.skillPoint--;
        }
        this.showInfo();
    }

    upgradeSkill(name){
        if(name === 1){
            if(this.attackDamageLevel < this.attackDamageMaxLevel){
                this.attackDamageLevel++;
                this.removeSkillPoint();
            }
        }
        else if(name === 2){
            if(this.healthPointIncreaseLevel < this.healthPointIncreaseMaxLevel){
                this.healthPointIncreaseLevel++;
                this.stat.healthPointIncrease();
                this.removeSkillPoint();
            }
        }
        else if(name === 3){
            if(this.attackLevel < this.attackMaxLevel){
                this.attackLevel++;
                this.removeSkillPoint();
            }
        }
        this.showInfo();
    }

    showSkillBox(){
        if(this.skillPoint > 0){
            if(this.attackDamageLevel < this.attackDamageMaxLevel){
                $("#skillBox1").removeClass();
                $("#skillBox1").addClass("boxOn");
            }
            if(this.healthPointIncreaseLevel < this.healthPointIncreaseMaxLevel){
                $("#skillBox2").removeClass();
                $("#skillBox2").addClass("boxOn");
            }
            if(this.attackLevel < this.attackMaxLevel){
                $("#skillBox3").removeClass();
                $("#skillBox3").addClass("boxOn");
            }
        }
        else{
            $("#skillBox1").removeClass();
            $("#skillBox2").removeClass();
            $("#skillBox3").removeClass();
        }
    }
    
    showSkillPoint(){
        let sp = document.getElementById("skill-point");
        sp.innerHTML = `point : ${this.skillPoint}`;
    }

    showInfo(){
        this.showSkillBox();
        this.showSkillPoint();
        this.showSkillLevel();
    }

    showSkillLevel(){
        let s1 = (document).querySelector("#skill1 a");
        let s2 = (document).querySelector("#skill2 a");
        let s3 = (document).querySelector("#skill3 a");
        if(this.attackDamageLevel < this.attackDamageMaxLevel){
            s1.innerHTML = `LV.${this.attackDamageLevel}`;
        }
        else{
            s1.innerHTML = `LV.MAX`;
        }
        if(this.healthPointIncreaseLevel < this.healthPointIncreaseMaxLevel){
            s2.innerHTML = `LV.${this.healthPointIncreaseLevel}`;
        }
        else{
            s2.innerHTML = `LV.MAX`;
        }
        if(this.attackLevel < this.attackMaxLevel){
            s3.innerHTML = `LV.${this.attackLevel}`;
        }
        else{
            s3.innerHTML = `LV.MAX`;
        }
    }
}
