const global_canvasWidth = 1600;
const global_canvasHeight = 800;

const global_canvas = new Canvas("A");



const IMG_background = new Image();

const IMG_bossEffect_timer = new Image();
const IMG_bossEffect_armor = new Image();

const IMG_monster_spritesheet = new Image();
const IMG_bossMonster_spritesheet = new Image();

const IMG_character_spritesheet = new Image();

const IMG_monsterAttack3 = new Image();
const IMG_monsterAttack6 = new Image();

IMG_background.src = 'css/images/map1/background2-1.png';
IMG_bossEffect_timer.src = 'css/images/others/timer.png';
IMG_bossEffect_armor.src = 'css/images/others/armor.png';
IMG_monster_spritesheet.src = 'css/images/monsters/monsters.png';
IMG_bossMonster_spritesheet.src = 'css/images/monsters/boss-monsters.png';
IMG_character_spritesheet.src = 'css/images/characters/spritechar.png';
IMG_monsterAttack3.src = 'css/images/monsterAttacks/monsterAttack3.png';
IMG_monsterAttack6.src = 'css/images/monsterAttacks/monsterAttack6.png';
