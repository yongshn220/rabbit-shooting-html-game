class Stats{
    name;
    fullHealthPoint;
    curHealthPoint;
    moveSpeed;
    skill;
    life;

    constructor(name, life){
        this.name = name;
        this.fullHealthPoint = 100;
        this.curHealthPoint = 100;
        this.moveSpeed = 2;
        this.skill = new Skill(name, this);
        this.showHealth();
        this.state = true;

    }
    gainHealthPoint(n){
        if(this.curHealthPoint + n >= this.fullHealthPoint){
            this.curHealthPoint = this.fullHealthPoint;
        }
        else{
            this.curHealthPoint += n;
        }
        this.showHealth();
    }

    
    damaged(AD){
        var h = this.curHealthPoint - AD;
        if(h <= 0){
            this.curHealthPoint = 0;
            this.state = false; //dead
        }
        else{
            this.curHealthPoint = h;
        }
        this.showHealth();
    }

    showHealth(){
        var curHealthBar = global_canvasWidth * (this.curHealthPoint / this.fullHealthPoint);
        $("#health" + this.name).css("width", curHealthBar);
    }

    checkState(){
        //this.checkHealthPointLevel();
        return this.state;
    }
    healthPointIncrease(){
        this.fullHealthPoint = this.fullHealthPoint + 80;
        this.showHealth();
    }
}