class Background{
    image;
    x;
    y;
    width;
    height;
    wallMap;

    constructor(src){
        this.x = 0;
        this.y = 0;
        this.width = global_canvasWidth;
        this.height = global_canvasHeight;
        this.image = IMG_background;
        this.wallMap = Array.from({length : 50}, () => Array(50).fill(false));
    }
    move(direction){
        if(direction === "right"){
            this.x = this.x + 1;
        }
        else if(direction === "left"){
            this.x = this.x - 1;
        }
        else if(direction === "up"){
            this.y = this.y - 1;
        }
        else if(direction === "down"){
            this.y = this.y + 1;
        }
    }
    drawBackground(){
        global_canvas.context.drawImage(
            this.image, 
            this.x, 
            this.y, 
            global_canvas.width, 
            global_canvas.height, 
            0, 
            0, 
            global_canvas.width, 
            global_canvas.height
        );
    }
}