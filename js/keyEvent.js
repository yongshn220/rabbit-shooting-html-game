var KEY1 = {
    //player one
    LEFT : 37,
    UP : 38,
    RIGHT : 39,
    DOWN : 40,
    ATTACK : 65,  //a
    skill1 : 49,
    skill2 : 50,
    skill3 : 51,
}

var keyOneMap = {
    [KEY1.LEFT] : false,
    [KEY1.UP] : false,
    [KEY1.RIGHT] : false,
    [KEY1.DOWN] : false,
    [KEY1.ATTACK] : false,
}

let keyToSkill = {
    [KEY1.skill1] : 1,
    [KEY1.skill2] : 2,
    [KEY1.skill3] : 3,
}


function moveCharacter(player){
    var tempCx = player.character.dx;
    var tempCy = player.character.dy;
    var moveSpeed = player.character.stats.moveSpeed;
    let der = Math.sqrt(moveSpeed**2 / 2);
    var keyStateLeft = keyOneMap[KEY1.LEFT];
    var keyStateRight = keyOneMap[KEY1.RIGHT];
    var keyStateUp = keyOneMap[KEY1.UP];
    var keyStateDown = keyOneMap[KEY1.DOWN];
    
    // if all key false
    if(!keyStateLeft && !keyStateRight && !keyStateUp && !keyStateDown){
        player.character.isMoving = false;
        return;
    }
    //left
    if(keyStateLeft && !keyStateUp && !keyStateRight && !keyStateDown){
        tempCx = tempCx - moveSpeed;
    }
    //up
    else if(keyStateUp && !keyStateLeft && !keyStateRight && !keyStateDown){
        tempCy = tempCy - moveSpeed;
    }
    //right
    else if(keyStateRight && !keyStateLeft && !keyStateUp && !keyStateDown){
        tempCx = tempCx + moveSpeed;
    }
    //down
    else if(keyStateDown && !keyStateLeft && !keyStateRight && !keyStateUp){
        tempCy = tempCy + moveSpeed;
    }
    //down-left
    else if(keyStateDown && keyStateLeft && !keyStateRight && !keyStateUp){
        tempCy = tempCy + der;
        tempCx = tempCx - der;
    }
    //down-right
    else if(keyStateDown && !keyStateLeft && keyStateRight && !keyStateUp){
        tempCy = tempCy + der;
        tempCx = tempCx + der;
    }
    //up-left
    else if(!keyStateDown && keyStateLeft && !keyStateRight && keyStateUp){
        tempCy = tempCy - der;
        tempCx = tempCx - der;
    }
    //up-right
    else if(!keyStateDown && !keyStateLeft && keyStateRight && keyStateUp){
        tempCy = tempCy - der;
        tempCx = tempCx + der;
    }
    //down-left-right
    else if(keyStateDown && keyStateLeft && keyStateRight && !keyStateUp){
        tempCy = tempCy + moveSpeed;
    }
    
    player.character.viewUpdate(getViewpoint(keyStateLeft, keyStateRight, keyStateUp, keyStateDown));

    if(player.isValid(tempCx, tempCy)){
        //nothing;
    }
    else{
        player.character.isMoving = false;
        return;

    }

    setTimeout(function(){
        moveCharacter(player);
    }, 5);
}

function getViewpoint(l, r, u, d){
    let viewpoint = ""; 
    if(l && !r && !u && !d){
        viewpoint = "left";
    }
    else if(!l && r && !u && !d){
        viewpoint = "right";
    }
    else if(!l && !r && u && !d){
        viewpoint = "up";
    }
    else if(!l && !r && !u && d){
        viewpoint = "down";
    }
    else if(l && !r && u && !d){
        viewpoint = "up-left";
    }
    else if(!l && r && u && !d){
        viewpoint = "up-right";
    }
    else if(l && !r && !u && d){
        viewpoint = "down-left";
    }
    else if(!l && r && !u && d){
        viewpoint = "down-right";
    }
    else{
        viewpoint = "down";
    }
    return viewpoint;
}

function attack(player){
    var keyState;
    keyState = keyOneMap[KEY1.ATTACK];
    if(keyState){
        player.attackEvent();
        //console.log("attack");
        setTimeout(function(){
            attack(player);
        }, 50);
    }
    return;
}

function skillEvent(keyCode){
    if(player1.character.stats.skill.skillPoint > 0){
        player1.character.stats.skill.upgradeSkill(keyToSkill[keyCode]);
    }
}

function addEventListener(){
    document.addEventListener('keydown', event => {
        if(event.keyCode in keyOneMap || event.keyCode in keyToSkill){
            if(!player1.isEnd){
                if(event.keyCode === KEY1.skill1 || event.keyCode === KEY1.skill2 || event.keyCode === KEY1.skill3){
                    skillEvent(event.keyCode);
                }

                keyOneMap[event.keyCode] = true;
                if(event.keyCode === KEY1.ATTACK && !player1.character.isAttacking){
                    player1.character.isAttacking = true;
                    attack(player1);
                }
                if(!player1.character.isMoving){
                    player1.character.isMoving = true;
                    moveCharacter(player1);
                }
            }
        }
    })
    document.addEventListener('keyup', event => {
        if(event.keyCode in keyOneMap){
            keyOneMap[event.keyCode] = false;
            if(event.keyCode === KEY1.ATTACK){
                player1.character.isAttacking = false;
            }
        }
    })
}