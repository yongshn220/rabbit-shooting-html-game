
class Player{
    isEnd;
    totalScore;
    name;
    character;
    attacks;
    baseBackground;
    middleBackground;
    topBackground;
    items;
    life;
    monstersOnScreen;
    stage;
    
    constructor(name){
        this.baseBackground = new Background();
        this.isEnd = false;
        this.totalScore = 0;
        this.name = name;
        this.character = new Character(name, this.baseBackground);
        this.isKeyPressed = false;
        this.attacks = [];
        this.items = [];
        this.monstersOnScreen = [];
        this.life = 200;
        this.stage = new Stage(this.character);
    }

    newMonster(level){
        if(!this.stage.gapTime){
            //보스 스테이지
            if(this.stage.isBossStage === true){
                let nextMonster = this.stage.getNextMonsterFromList();
                if(nextMonster){
                    this.monstersOnScreen.push(nextMonster);
                }
                this.monstersOnScreen.push(this.stage.getMiniMonster());
                return true;
            }
            //일반 스테이지
            else{
                let nextMonster = this.stage.getNextMonsterFromList();
                if(nextMonster){
                    this.monstersOnScreen.push(nextMonster);
                    this.stage.addScore(nextMonster.score);
                    return true;
                }
            }
        }
        else{
            return false;
        }
    }
    
    monstersImageUpdate(){
        this.monstersOnScreen.forEach(monster => monster.imageUpdate());
    }
    monstersMove(){
        this.monstersOnScreen.forEach(monster => monster.move());
    }
    monstersFilterDeads(){
        this.monstersOnScreen = this.monstersOnScreen.filter(monster => !monster.isDead);
    }
    monstersAttackEvent(){
        this.monstersOnScreen.forEach(monster => monster.attack());
    }
    monstersDrawAttacks(){
        this.monstersOnScreen.forEach(monster => monster.drawAttacks());
    }

    monstersEvents(){
        this.monstersOnScreen.forEach(monster => monster.events());
        this.monstersOnScreen = this.monstersOnScreen.filter(monster => !monster.isDead);
        this.bossMonsterEffect();
    }

    bossMonsterEffect(){
        if(this.stage.isBossStage){
            if(this.stage.curStage === 10){
                this.monstersOnScreen.forEach(m => {
                    if(m.name === "boss"){

                    }
                    else{
                        global_canvas.context.drawImage(IMG_bossEffect_timer, 0, 0, 20, 20, m.dx, m.dy - 20, 20, 20);
                        //global_canvas.context.drawImage(IMG_bossEffect_armor, 0, 0, 20, 20, m.dx + 25, m.dy - 20, 20, 20);
                    }
                })
            }
        }
    }
    
    isMonsterAttackHitCharacter(){
        let x = this.character.dx + 40;
        let y = this.character.dy + 40;
        let w = this.character.dw / 2;
        let h = this.character.dh / 2;
        let ax;
        let ay;
        let aw;
        let ah;
        
        this.monstersOnScreen.forEach(monster => {
            monster.attacks.forEach(attack => {
                ax = attack.dx;
                ay = attack.dy;
                aw = attack.dw;
                ah = attack.dh;

                if(!this.character.isDamaged){
                    if(((x + w) >= ax) && (x <= (ax + aw)) && ((y + h) >= ay) && (y <= (ay + ah))){
                        attack.hit = true;
                        this.character.isDamaged = true;
                        this.character.damaged(attack.damage);
                        return;
                    }
                }
            });
        });    
    }

    isAllCleared(){
        if(this.monstersOnScreen.length <= 0){
            return true;
        }
        return false;
    }
    addScore(score){
        this.totalScore += score;
    }
    lifeDecrease(){
        this.life--;
        if(this.life < 0){
            this.gameEndEvent();
        }
    }
 
    // item events
    newItem(name){
        this.items.push(new Item(name));
        
    }

    isItemCollied(item){
        var ix = item.x;
        var iy = item.y;
        var iw = item.width;
        var ih = item.height;
        var cx = this.character.dx;
        var cy = this.character.dy;
        var cw = this.character.dw;
        var ch = this.character.dh;;

        if(((ix + iw) >= cx) && (ix <= (cx + cw)) && ((iy + ih) >= cy) && (iy <= (cy + ch))){
            return true;
        }
        return false;
    }

    isValid(cx, cy){
        var checkX = false;
        var checkY = false;
        if(this.isCharacterInsideCanvas(cx, this.character.dy)){
            this.character.dx = cx;
            checkX = true;
        }
        if(this.isCharacterInsideCanvas(this.character.dx, cy)){
            this.character.dy = cy;
            checkY = true;
        }
        
        if(!checkX && !checkY){
            return false;
        }
        return true;  
    }

    isCharacterInsideCanvas(x, y){
        if(x >= 0 &&  y >= 0 && x < global_canvas.width - this.character.sw && y < global_canvas.height - this.character.sh){
            return true;
        }
        return false;
    }

    characterAndMonsterNotCollied(){
        let isCollied = false;
        let x = this.character.dx + 40;
        let y = this.character.dy + 40;
        let w = 1;
        let h = 1;
        let mx;
        let my;
        let mw;
        let mh;
        
        this.monstersOnScreen.forEach(monster => {
            mx = monster.dx;
            my = monster.dy;
            mw = monster.dw;
            mh = monster.dh;
            
            if(!this.character.isDamaged){
                if(((x + w) >= mx) && (x <= (mx + mw)) && ((y + h) >= my) && (y <= (my + mh))){
                    this.character.isDamaged = true;
                    isCollied = true;
                    this.character.damaged(monster.attackDamage);
                    return;
                }
            }
        });    
        return isCollied;
    }

    isAttackHit(attack, dir){
        let isHit = false;
        let AD = attack.attackDamage;
        let ax = attack.x;
        let ay = attack.y;
        let aw = attack.aw;
        let ah = attack.ah;

        let mx;
        let my;
        let mw;
        let mh;
    
        this.monstersOnScreen.forEach(monster => {
            mx = monster.dx;
            my = monster.dy;
            mw = monster.dw;
            mh = monster.dh;
            if(((ax + aw) >= mx) && (ax <= (mx + mw)) && ((ay + ah) >= my) && (ay <= (my + mh))){
                this.attackHitEvent(AD, monster, dir);
                isHit = true;
                return;
            }
        });
        
        return isHit;
    }
    

    attackHitEvent(AD, monster, dir){
        this.attackHitEventDamaged(AD, monster);
    }

    attackHitEventDamaged(AD, monster){
        let score = monster.damaged(AD);
        if(score === -1){
            this.stage.isBossStage = false;
        }
        else{
            this.addScore(score);
        }
    }

    attackEvent(){
        var x = this.character.dx;
        var y = this.character.dy;
        let level = this.character.stats.skill.attackLevel;
        let vp = this.AttackPoisitionWithCharacterView(this.character.view);
        let vf = this.AttackFrontPositionWithCharacterView(this.character.view);
        let cvc1 = this.characterViewChangeOne(this.character.view);
        let cvc2 = this.characterViewChangeTwo(this.character.view);
        
        if(level === 1){
            this.attacks.push(new Attack(x, y, this.character.view, this.character.stats.skill, 1));
        }
        else if(level === 2){
            this.attacks.push(new Attack(x - 10*vp[0], y - 10*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 10*vp[0], y + 10*vp[1], this.character.view, this.character.stats.skill, 1));
        }   
        else if(level === 3){
            this.attacks.push(new Attack(x - 20*vp[0], y - 20*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 20*vf[0], y + 20*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 20*vp[0], y + 20*vp[1], this.character.view, this.character.stats.skill, 1));
        } 
        else if(level === 4){
            this.attacks.push(new Attack(x - 25*vp[0], y - 25*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0] + 10*vf[0], y - 15*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 20*vf[0], y + 20*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 20*vp[0] + 10*vf[0], y + 20*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 30*vp[0], y + 30*vp[1], this.character.view, this.character.stats.skill, 1));
        }   
        else if(level === 5){
            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 25*vp[0] + 5*vf[0], y - 25*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0] + 10*vf[0], y - 15*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 20*vf[0], y + 20*vf[1], this.character.view, this.character.stats.skill, 2  ));
            this.attacks.push(new Attack(x + 20*vp[0] + 10*vf[0], y + 20*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 30*vp[0] + 5*vf[0], y + 30*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 40*vp[0], y + 40*vp[1], this.character.view, this.character.stats.skill, 1));
        }    
        else if(level === 6){
            this.attacks.push(new Attack(x, y, cvc1[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 25*vp[0] + 5*vf[0], y - 25*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0] + 10*vf[0], y - 15*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 20*vf[0], y + 20*vf[1], this.character.view, this.character.stats.skill, 3));
            this.attacks.push(new Attack(x + 20*vp[0] + 10*vf[0], y + 20*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 30*vp[0] + 5*vf[0], y + 30*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 40*vp[0], y + 40*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc1[1], this.character.stats.skill, 1));
        }
        else if(level === 7){
            this.attacks.push(new Attack(x, y, cvc2[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc1[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 25*vp[0] + 5*vf[0], y - 25*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0] + 10*vf[0], y - 15*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 20*vf[0], y + 20*vf[1], this.character.view, this.character.stats.skill, 3));
            this.attacks.push(new Attack(x + 20*vp[0] + 10*vf[0], y + 20*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 30*vp[0] + 5*vf[0], y + 30*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 40*vp[0], y + 40*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc1[1], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc2[1], this.character.stats.skill, 1));
        }
        else if(level === 8){
            this.attacks.push(new Attack(x - 15*vp[0], y - 15*vp[1], cvc2[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc2[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 15*vp[0], y + 15*vp[1], cvc2[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0], y - 15*vp[1], cvc1[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc1[0], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 15*vp[0], y + 15*vp[1], cvc1[0], this.character.stats.skill, 1));

            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 25*vp[0] + 5*vf[0], y - 25*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0] + 10*vf[0], y - 15*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 20*vf[0], y + 20*vf[1], this.character.view, this.character.stats.skill, 3));
            this.attacks.push(new Attack(x + 20*vp[0] + 10*vf[0], y + 20*vp[1] + 10*vf[1], this.character.view, this.character.stats.skill, 2));
            this.attacks.push(new Attack(x + 30*vp[0] + 5*vf[0], y + 30*vp[1]  + 5*vf[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 40*vp[0], y + 40*vp[1], this.character.view, this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 35*vp[0], y - 35*vp[1], this.character.view, this.character.stats.skill, 1));
            
            this.attacks.push(new Attack(x - 15*vp[0], y - 15*vp[1], cvc2[1], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc2[1], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 15*vp[0], y + 15*vp[1], cvc2[1], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x - 15*vp[0], y - 15*vp[1], cvc1[1], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x, y, cvc1[1], this.character.stats.skill, 1));
            this.attacks.push(new Attack(x + 15*vp[0], y + 15*vp[1], cvc1[1], this.character.stats.skill, 1));
        }
    }

    characterViewChangeOne(view){
        if(view === "left"){
            return ["up-left", "down-left"];
        }
        else if(view === "right"){
            return ["up-right", "down-right"];
        }
        else if(view === "up"){
            return ["up-left", "up-right"];
        }
        else if(view === "down"){
            return ["down-left", "down-right"];
        }
        else if(view === "up-left"){
            return ["left", "up"];
        }
        else if(view === "up-right"){
            return ["up", "right"];
        }
        else if(view === "down-left"){
            return ["left", "down"];
        }
        else if(view === "down-right"){
            return ["right", "down"];
        }
    }

    characterViewChangeTwo(view){
        if(view === "left"){
            return ["up-right", "down-right"];
        }
        else if(view === "right"){
            return ["up-left", "down-left"];
        }
        else if(view === "up"){
            return ["down-right", "down-left"];
        }
        else if(view === "down"){
            return ["up-right", "up-left"];
        }
        else if(view === "up-left"){
            return ["right", "down"];
        }
        else if(view === "up-right"){
            return ["down", "left"];
        }
        else if(view === "down-left"){
            return ["right", "up"];
        }
        else if(view === "down-right"){
            return ["left", "up"];
        }
    }

    AttackPoisitionWithCharacterView(view){
        let x;
        let y;
        switch(view){
            case "left" : x = 0; y = 1;
            break;
            case "right" : x = 0; y = 1;
            break;
            case "up" : x = 1; y = 0;
            break;
            case "down" : x = 1; y = 0;
            break;
            case "up-left" : x = -1; y = 1;
            break;
            case "up-right" : x = 1; y = 1;
            break;
            case "down-left" : x = 1; y = 1;
            break;
            case "down-right" : x = -1; y = 1;
            break;
        }
        return [x,y];
    }

    AttackFrontPositionWithCharacterView(view){
        let x;
        let y;
        switch(view){
            case "left" : x = -1; y = 0;
            break;
            case "right" : x = 1; y = 0;
            break;
            case "up" : x = 0; y = -1;
            break;
            case "down" : x = 0; y = 1;
            break;
            case "up-left" : x = -1; y = -1;
            break;
            case "up-right" : x = 1; y = -1;
            break;
            case "down-left" : x = -1; y = 1;
            break;
            case "down-right" : x = 1; y = 1;
            break;
        }
        return [x,y];
    }
    isAttackInsideMap(x, y, w, h){
        if(x > 0 && (x + w) < this.baseBackground.width && y > 0 && (y + h) < this.baseBackground.height){
            return true;
        }
        return false;
    }

    isAttackInsideRange(sx, sy, x, y, range){
        Math.sqrt((x - sx)**2 + (y - sy)**2)
        if(Math.sqrt((x - sx)**2 + (y - sy)**2) < range){
            return true;
        }
        return false;
    }

    isAttackValid(attack, dir){
        var sx = attack.sx;
        var sy = attack.sy;
        var x = attack.x;
        var y = attack.y;
        var w = attack.aw;
        var h = attack.ah;
        if(this.isAttackInsideMap(x, y, w, h) && this.isAttackInsideRange(sx, sy, x, y, attack.range)){
            if(this.isAttackHit(attack, dir)){
                return false;
            }
            return true;
        }
        return false;
    }

    showAttacks(){
        for(var i = 0; i < this.attacks.length; i++){
            var curAttack = this.attacks[i];   
            if(this.isAttackValid(curAttack, curAttack.direction)){
                curAttack.move();
                curAttack.drawAttack();
            }
            else{ // if attack hits something
                this.attacks.splice(i, 1);
            }
        }
    }

    attacksReposition(){
        for(var i = 0; i < this.attacks.length; i++){
            this.attacks[i].reposition(this.baseBackground.x, this.baseBackground.y);
        }
    }

    getItem(item){
        if(item.name === "health"){
            this.character.stats.gainHealthPoint(10);
        }
    }

    showItems(){
        for(var i = 0; i < this.items.length; i++){
            var curItem = this.items[i];
            curItem.drawItem();
            if(this.isItemCollied(curItem)){
                this.getItem(curItem);
                
                this.items.splice(i, 1);               
            } 
        }
    }

    showBaseBackground(){
        this.baseBackground.drawBackground();
    }

    showMiddleBackground(){
    }

    showTopBackground(){
    }

    showCharacter(){
        this.character.drawCharacter();
    }

    showMonsters(){
        this.monstersOnScreen.forEach(monster => monster.drawMonster());
    }

    showTexts(){
        let txt = `score : ${this.totalScore}`;
        global_canvas.context.font = "30px Arial";
        global_canvas.context.fillStyle = "blue";
        global_canvas.context.textAlign = "center";
        global_canvas.context.fillText(txt, 800, 30);
    }

    checkState(){
        return this.character.stats.checkState();
    }
}


