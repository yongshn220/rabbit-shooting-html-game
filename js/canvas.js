class Canvas{
    x;
    y;
    width;
    height;
    element;
    context;
    
    constructor(name){
      this.x = 0;
      this.y = 0;
      this.width = 1600;
      this.height = 800;
      this.element = document.getElementById('canvas' + name);
      this.element.width = this.width;
      this.element.height = this.height;
      this.context = this.element.getContext('2d');
    }

    clearCanvas(){
        this.context.clearRect(0, 0, this.width, this.height);
    }

}